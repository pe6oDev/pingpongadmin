@extends('layout')

@section('content')
    <h2>{{$venue->name}} - Tables</h2>

    <div class="row">
        @foreach($tables as $table)
            <div class="col">
                <div class="card" style="width: 18rem; margin-top: 10px;">
                    <img class="card-img-top" src="#" alt="{{$table->name}}">
                    <div class="card-body">
                        <h5 class="card-title">{{$table->name}}</h5>
                        <p class="card-text"><i>{{$venue->name}}</i></p>
                        <a href="{{route('getBookingSlots', [$venue->id, $table->id])}}" class="btn btn-primary">Book</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
