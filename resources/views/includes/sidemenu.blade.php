<ul class="nav flex-column col-2">
    <li class="nav-item">
        <a class="nav-link" href="{{route('getDashboard')}}">Dashboard</a>
    </li>
    @if(!isset($venues) && isset($venue))
        <li class="nav-item">
            <a class="nav-link" href="{{route('getVenue', [$venue->id])}}">Tables</a>
        </li>
    @endif
    <li class="nav-item">
        <a class="nav-link" href="{{route('getBookings')}}">Bookings</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('logout')}}">Logout</a>
    </li>
</ul>
