@extends('admin.layout')

@section('content')
    @if($errors->any())
        <div class="errorProfilePage">
            <span style="font-size: 25px"><b>Errors: </b></span>
            <ul class="alert-box warning radius">
                @foreach($errors->all() as $error)
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
    @endif
    @foreach($days as $day_id=>$day)
        <h4>{{$day}}</h4>
        <form action="{{route('admin.postVenueSettings', [$venue->id])}}" method="post">
            <div class="form-group w-50">
                <label >Opening time</label>
                <input
                    type="text"
                    class="form-control"
                    name="openingTime"
                    value="{{$venue->openingTime()->where('day', $day_id)->first()->opening_time}}"
                    placeholder="Enter time"
                >
            </div>
            <div class="form-group w-50">
                <label >Closing time</label>
                <input
                    type="text"
                    class="form-control"
                    name="closingTime"
                    value="{{$venue->openingTime()->where('day', $day_id)->first()->closing_time}}"
                    placeholder="Enter time"
                >
            </div>
            <div class="form-check">
                <input
                    type="checkbox"
                    class="form-check-input"
                    name="isWorking"
                    @if($venue->openingTime()->where('day', $day_id)->first()->is_working) checked="checked" @endif
                >
                <label class="form-check-label" for="exampleCheck1">Is it working</label>
            </div>
            <input type="hidden" name="day" value="{{$day_id}}">
            @csrf
            <button type="submit" class="btn btn-primary mt-4">Submit</button>
        </form>
    @endforeach
@endsection
