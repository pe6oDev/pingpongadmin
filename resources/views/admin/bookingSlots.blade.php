@extends('admin.layout')

@section('content')
    <h2>{{$venue->name}} - {{$table->name}}</h2>

    <p>{{$today}}</p>
    <input
        class="datepicker"
        data-provide="datepicker"
        data-url="{{route('admin.newDate')}}"
        data-venue="{{$venue->id}}"
        data-venue-name="{{$venue->name}}"
        data-table="{{$table->id}}"
        data-table-name="{{$table->name}}"
    >
    <div class="row slots">
        @foreach($slots as $slot)
            <div class="col">
                <div class="card" style="width: 18rem; margin-top: 10px;" id="{{$slot['start']}}">
                    <img class="card-img-top" src="#" alt="{{date('H:i', $slot['start'])}}-{{date('H:i', $slot['end'])}}">
                    <div class="card-body">
                        <h5 class="card-title">{{date('H:i', $slot['start'])}}-{{date('H:i', $slot['end'])}}</h5>
                        <p class="card-text"><i>{{$venue->name}} - {{$table->name}}</i></p>
                        <button type="button"
                                class="btn btn-primary showModalForm"
                                data-table="{{$table->id}}"
                                data-start="{{$slot['start']}}"
                                data-start-text="{{date('H:i', $slot['start'])}}"
                                data-end="{{$slot['end']}}"
                                data-end-text="{{date('H:i', $slot['end'])}}"
                        >
                            Book
                        </button>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="modal fade" id="modalForm" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('admin.bookTable')}}" class="formBooking" method="post">
                    <div class="modal-body">
                        <div class="form-group w-50">
                            <label >Name</label>
                            <input
                                type="text"
                                class="form-control"
                                name="name"
                                id="name"
                                value=""
                                placeholder="Enter name"
                            >
                        </div>
                        <div class="form-group w-50">
                            <label></label>Phone</label>
                            <input
                                type="text"
                                class="form-control"
                                name="phone"
                                id="phone"
                                value=""
                                placeholder="Enter phone number"
                            >
                        </div>
                        <div class="form-group w-50">
                            <label>Email</label>
                            <input
                                type="text"
                                class="form-control"
                                name="email"
                                id="email"
                                value=""
                                placeholder="Enter email"
                            >
                        </div>
                        <input type="hidden" name="table" id="tableField" value="">
                        <input type="hidden" name="start" id="startField" value="">
                        <input type="hidden" name="end" id="endField" value="">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary submitModalForm">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
