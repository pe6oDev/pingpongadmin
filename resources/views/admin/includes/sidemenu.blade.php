<ul class="nav flex-column col-2">
    <li class="nav-item">
        <a class="nav-link" href="{{route('admin.getVenues')}}">Venues</a>
    </li>
    @if(!isset($venues) && isset($venue))
        <li class="nav-item">
            <a class="nav-link" href="{{route('admin.getVenueSettings', [$venue->id])}}">Venue settings</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('admin.getVenue', [$venue->id])}}">Table</a>
        </li>
    @endif
    <li class="nav-item">
        <a class="nav-link" href="{{route('admin.getBookings')}}">Bookings</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('admin.logout')}}">Logout</a>
    </li>
</ul>
