@extends('layout')

@section('content')
    <h2>Bookings</h2>

    <div class="row">
        @foreach($bookings as $booking)
            <div class="col">
                <div class="card" style="width: 18rem; margin-top: 10px;">
                    <img class="card-img-top" src="#" alt="{{date('H:i', $booking->booking_start)}}-{{date('H:i', $booking->booking_end)}}">
                    <div class="card-body">
                        <h5 class="card-title">
                            {{date('H:i', $booking->booking_start)}}-{{date('H:i', $booking->booking_end)}} {{date('d.m.Y', $booking->booking_start)}}
                        </h5>
                        <p class="card-text"><b>{{$booking->table()->first()->venue()->first()->name}}</b><i> - {{$booking->table()->first()->name}}</i></p>
                        <button data-url="{{route('admin.getDeleteBooking', [$booking->id])}}" class="btn btn-primary deleteBooking">Delete</button>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
