@extends('admin.layout')

@section('content')
    <h2><a href="{{route('admin.getVenueSettings', [$venue->id])}}">Go to settings</a></h2>

    <h2>Tables</h2>

    <div class="row">
        @foreach($tables as $table)
            <div class="col">
                <div class="card" style="width: 18rem; margin-top: 10px;">
                    <img class="card-img-top" src="#" alt="{{$table->name}}">
                    <div class="card-body">
                        <h5 class="card-title">{{$table->name}}</h5>
                        <p class="card-text"><i>{{$venue->name}}</i></p>
                        <a href="{{route('admin.getBookingSlots', [$venue->id, $table->id])}}" class="btn btn-primary">Book</a>
                    </div>
                    <button type="button" class="deleteTable" data-url="{{route('admin.getDeleteTable', [$table->id])}}" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endforeach
            <div class="col">
                <div class="card" style="width: 18rem; margin-top: 10px;">
                    <img class="card-img-top" src="#" alt="+">
                    <div class="card-body">
                        <h5 class="card-title">Add a new table</h5>
                        <p class="card-text"><i>{{$venue->name}}</i></p>
                        <a href="{{route('admin.getAddTable', [$venue->id])}}" class="btn btn-primary">Add</a>
                    </div>
                </div>
            </div>
    </div>
@endsection
