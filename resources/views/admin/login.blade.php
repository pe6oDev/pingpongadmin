<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login</title>
    <link rel="stylesheet" href={{ asset('css/bootstrap/bootstrap.min.css') }}>
</head>
<body>
<div class="container mt-5">
    @if($errors->any())
        <div class="errorProfilePage">
            <span style="font-size: 25px"><b>Errors: </b></span>
            <ul class="alert-box warning radius">
                @foreach($errors->all() as $error)
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{route('admin.validate_login')}}" method="post">
        <div class="form-group w-50">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" name="email" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group w-50">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" name="password" placeholder="Password">
        </div>
        @csrf
        <button type="submit" class="btn btn-primary mt-4">Submit</button>
    </form>
</div>
<script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
</body>
</html>
