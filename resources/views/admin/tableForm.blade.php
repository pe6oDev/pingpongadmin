@extends('admin.layout')

@section('content')
    @if($errors->any())
        <div class="errorProfilePage">
            <span style="font-size: 25px"><b>Errors: </b></span>
            <ul class="alert-box warning radius">
                @foreach($errors->all() as $error)
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{route('admin.postAddTable', [$venue->id])}}" method="post">
        <div class="form-group w-50">
            <label >Name of table</label>
            <input type="text" class="form-control" name="name" placeholder="Enter name">
        </div>
        <div class="form-group w-50">
            <label >Length of booking</label>
            <input type="number" class="form-control" name="lengthOfBooking" placeholder="Enter length">
        </div>
        @csrf
        <button type="submit" class="btn btn-primary mt-4">Submit</button>
    </form>
@endsection
