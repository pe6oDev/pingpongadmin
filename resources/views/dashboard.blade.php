@extends('layout')

@section('content')
    <h2>Hello, {{$user->name}}</h2>

    <div class="container">
        <div class="row">
            @foreach($venues as $venue)
                <div class="col">
                    <div class="card" style="width: 18rem; margin-top: 10px;">
                        <img class="card-img-top" src="#" alt="{{$venue->name}}">
                        <div class="card-body">
                            <h5 class="card-title">{{$venue->name}}</h5>
                            {{--                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
                            <a href="{{route('getVenue', [$venue->id])}}" class="btn btn-primary">View Tables</a>
                        </div>
                    </div>
                </div>
            @endforeach
    </div>
@endsection
