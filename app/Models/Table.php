<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'venue_id', 'length_of_booking', 'created_at', 'updated_at'
    ];

    public function venue(){
        return $this->belongsTo(Venue::class);
    }

    public function bookings(){
        return $this->hasMany(Booking::class);
    }

    public function deleteTable(){
        $bookings = $this->bookings()->get();
        foreach($bookings as $booking){
            $booking->delete();
        }
        return $this->delete();
    }
}
