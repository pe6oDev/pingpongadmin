<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VenuesOpeningTime extends Model
{
    use HasFactory;

    protected $fillable = [
        'day', 'opening_time', 'closing_time', 'is_working', 'created_at', 'update_at'
    ];
}
