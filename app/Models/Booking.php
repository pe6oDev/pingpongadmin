<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'table_id', 'booking_start', 'booking_end', 'user_id', 'phone', 'email', 'created_at', 'updated_at',
    ];

    public function table(){
        return $this->belongsTo(Table::class);
    }
}
