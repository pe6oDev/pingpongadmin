<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$roles
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {
        $found = false;
        foreach($roles as $role) {
            if (! $request->user()->hasRole($role)) continue;
            $found = true;
            break;
        }
        if(!$found)
            abort(401, 'This action is unauthorized.');
        return $next($request);
    }
}
