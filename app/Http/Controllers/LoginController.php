<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Show login form.
     *
     * @return view
     */
    public function show() {
        return view('login');
    }

    /**
     * Handle incoming login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function postLogin(Request $request)
    {
        //Validate
        $loginData = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($loginData)) {
            return redirect()->route('getDashboard');
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
