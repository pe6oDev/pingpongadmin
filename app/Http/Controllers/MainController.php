<?php

namespace App\Http\Controllers;

use App\Models\Venue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    /**
     * Show dashboard.
     *
     * @return view
     */
    public function getDashboard() {
        $venues = Venue::get();
        $user = Auth::user();

        return view('dashboard', ['pageName' => "Dashboard", 'venues' => $venues, 'user' => $user]);
    }

    /**
     * Show one venue with settings and tables.
     *
     * @param int $id
     * @return view
     */
    public function getVenue($id){
        $venue = Venue::where('id', $id)->first();
        $tables = $venue->tables()->get();

        return view('venue', ['pageName' => 'Add Venue', 'venue' => $venue, 'tables' => $tables]);
    }
}
