<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Table;
use App\Models\Venue;
use App\Models\VenuesOpeningTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VenuesController extends Controller
{
    /**
     * Show all venues page.
     *
     * @return view
     */
    public function getVenues()
    {
        $venues = Venue::get();

        return view('admin.venues', ['pageName' => 'Venues', 'venues' => $venues]);
    }

    /**
     * Show one venue with settings and tables.
     *
     * @param int $id
     * @return view
     */
    public function getVenue($id){
        $venue = Venue::where('id', $id)->first();
        $tables = $venue->tables()->get();

        return view('admin.venue', ['pageName' => 'Add Venue', 'venue' => $venue, 'tables' => $tables]);
    }

    /**
     * Show add venue page.
     *
     * @return view
     */
    public function getAddVenue(){
        return view('admin.venueForm', ['pageName' => 'Add Venue']);
    }

    /**
     * Handle request to add new venue.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return redirect
     */
    public function postAddVenue(Request $request){
        $data = $request->validate([
            'name' => ['required', 'max:256'],
        ]);

        $name = $_POST['name'];

        $venue = new Venue();
        $venue->name = $name;
        $venue->save();

        return redirect()->route('admin.getVenues');
    }

    /**
     * Show the page to edit a venue's settings.
     *
     * @param int $id
     * @return view
     */
    public function getVenueSettings($id){
        $venue = Venue::where('id', $id)->first();
        $days_of_week = [
            '1' => 'Monday',
            '2' => 'Tuesday',
            '3' => 'Wednesday',
            '4' => 'Thursday',
            '5' => 'Friday',
            '6' => 'Saturday',
            '7' => 'Sunday',
        ];

        if($venue->openingTime()->get()->isEmpty()){
             foreach($days_of_week as $day_id=>$day_name){
                 $openingTime = new VenuesOpeningTime();
                 $openingTime->venue_id = $venue->id;
                 $openingTime->day = $day_id;
                 $openingTime->opening_time = "00:00";
                 $openingTime->closing_time = "23:59";
                 $openingTime->is_working = 1;
                 $openingTime->save();
             }
        }

        return view('admin.venueSettings', ['pageName' => 'Settings', 'venue' => $venue, 'days' => $days_of_week]);
    }

    /**
     * Handle request to change venue settings.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return redirect
     */
    public function postVenueSettings(Request $request, $id){
        $data = $request->validate([
            'openingTime' => ['required', 'max:256'],
            'closingTime' => ['required', 'max:256'],
        ]);

        $venue = Venue::where('id', $id)->first();

        $openingTime = $_POST['openingTime'];
        $closingTime = $_POST['closingTime'];
        $day = $_POST['day'];
        if($_POST['isWorking']){
            $isWorking = 1;
        } else {
            $isWorking = 0;
        }

        $venueOpeningTime = $venue->openingTime()->where('day', $day)->first();
        $venueOpeningTime->opening_time = $openingTime;
        $venueOpeningTime->closing_time = $closingTime;
        $venueOpeningTime->is_working = $isWorking;
        $venueOpeningTime->save();

        return redirect()->route('admin.getVenueSettings', [$id]);
    }
}
