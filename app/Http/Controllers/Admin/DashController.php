<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashController extends Controller
{
    /**
     * Show dashboard page.
     *
     * @return view
     */
    public function index()
    {
        $user = Auth::user();

        return view('admin.dash', ['user' => $user, 'pageName' => 'Dashboard']);
    }
}
