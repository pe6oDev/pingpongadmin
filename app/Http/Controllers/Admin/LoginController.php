<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{


    /**
     * Show login form.
     *
     * @return view
     */
    public function show() {
        return view('admin.login');
    }

    /**
     * Handle incoming login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function validateAuth(Request $request) {
        //Validate
        $loginData = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        //Attempt login
        if ($this->guard()->attempt($loginData)) {
            $user = $this->guard()->user();
            //Check for role
            if(!$user->hasRole('Admin')){
                $this->logoutUser($request);
//                throw ValidationException::withMessages([
//                    $this->username() => [trans('admin/login.auth.role_failed')],
//                ]);
                return;
            }

            if (Auth::attempt($loginData)) {
                return redirect()->route('admin.dash');
            }
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
        return redirect()->route('admin.login');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

}
