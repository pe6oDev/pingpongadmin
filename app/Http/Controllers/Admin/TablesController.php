<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Table;
use App\Models\Venue;
use Illuminate\Http\Request;

class TablesController extends Controller
{
    /**
     * Show the page to add a new table.
     *
     * @param int $venue_id
     * @return view
     */
    public function getAddTable($venue_id){
        $venue = Venue::where('id', $venue_id)->first();

        return view('admin.tableForm', ['pageName' => 'Add table', 'venue' => $venue]);
    }

    /**
     * Handle request to add a new table.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $venue_id
     * @return redirect
     */
    public function postAddTable(Request $request, $venue_id)
    {
        $data = $request->validate([
            'name' => ['required', 'max:256'],
            'lengthOfBooking' => ['required', 'max:10'],
        ]);

        $venue = Venue::where('id', $venue_id)->first();

        $table = new Table();
        $table->venue_id = $venue->id;
        $table->name = $_POST['name'];
        $table->length_of_booking = $_POST['lengthOfBooking'];
        $table->save();

        return redirect()->route('admin.getVenue', [$venue->id]);
    }

    /**
     * Delete a table.
     *
     * @param int $id
     * @return redirect
     */
    public function getDeleteTable($id){
        $table = Table::where('id', $id)->first();
        $venue_id = $table->venue_id;

        $table->deleteTable();

        return redirect()->route('admin.getVenue', [$venue_id]);
    }
}
