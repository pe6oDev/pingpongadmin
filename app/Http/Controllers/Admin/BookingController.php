<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Table;
use App\Models\Venue;
use Facade\Ignition\Tabs\Tab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
    /**
     * Show the page for booking a slot on a table.
     *
     * @param int $venue_id
     * @param int $table_id
     * @return view
     */
    public function getBookingSlots($venue_id, $table_id){
        $venue = Venue::where('id', $venue_id)->first();
        $table = Table::where('id', $table_id)->first();
        $today = date('d.m.Y');

        $slots = $this->getSlots($venue, $table, $today);

        return view('admin.bookingSlots', [
            'pageName' => 'Booking slots',
            'venue' => $venue,
            'table' => $table,
            'slots' => $slots,
            'today' => $today
        ]);
    }

    /**
     * Handling ajax call to change the date of the booking.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function newDate(Request $request){
        $day = $request->input('day');
        $venue = Venue::where('id', $request->input('venue'))->first();
        $table = Table::where('id', $request->input('table'))->first();

        $slots = $this->getSlots($venue, $table, $day);

        return $slots;
    }

    /**
     * Handling ajax call to book a table.
     *
     * @param  App\Models\Venue $venue
     * @param  App\Models\Table $table
     * @param string $day
     * @return array
     */
    public function getSlots($venue, $table, $day){
        $dateTime = strtotime($day);
        $day_id = date('N', $dateTime);
        $currentTime = time();

        $workingTime = $venue->openingTime()->where('day', $day_id)->first();
        $bookingLength = $table->length_of_booking;

        $openingTime = strtotime("" . $day . " " . $workingTime->opening_time . "");
        $closingTime = strtotime("" . $day . " " . $workingTime->closing_time . "");

        $bookings = $table->bookings()->get();
        $bookingTimes = [];
        if(!$bookings->isEmpty()) {
            foreach ($bookings as $booking) {
                if($booking->booking_start < strtotime($day . " 23:59")){
                    $bookingTime = ['start' => $booking->booking_start, 'end' => $booking->booking_end];

                    array_push($bookingTimes, $bookingTime);
                }
            }
        }

        $slots = [];
        $startTime = $openingTime;
        while($startTime < $closingTime){
            $slot = ['start' => $startTime, 'end' => $startTime + $bookingLength*60];
            $showSlot = 1;
            if(!empty($bookingTimes)){
                foreach($bookingTimes as $bookingTime){
                    if($startTime >= $bookingTime['start'] && $startTime < $bookingTime['end']){
                        $showSlot = 0;
                    }
                }
            }
            if($startTime >= $currentTime  && $showSlot){
                array_push($slots, $slot);
            }
            $startTime = $startTime + $bookingLength*60;
        }

        return $slots;
    }
    /**
     * Handling ajax call to book a table.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return int
     */
    public function bookTable(Request $request){
        $data = $request->validate([
            'name' => ['required', 'max:256'],
            'phone' => ['required', 'max:20'],
            'email' => ['required', 'email'],
        ]);

        $name = $request->input('name');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $table_id = $request->input('table');
        $table = Table::where('id', $table_id)->first();
        $start = $request->input('start');
        $end = $request->input('end');

        $booking = new Booking();
        $booking->name = $name;
        $booking->phone = $phone;
        $booking->email = $email;
        $booking->table_id = $table_id;
        $booking->booking_start = $start;
        $booking->booking_end = $end;
        $booking->save();

        return $start;
    }

    /**
     * Show page with all bookings.
     *
     * @return view
     */
    public function getBookings(){
        $bookings = Booking::get();

        return view('admin.bookings', ['pageName' => 'Bookings', 'bookings' => $bookings]);
    }

    /**
     * Delete booking.
     *
     * @param int $booking_id
     * @return redirect
     */
    public function getDeleteBooking($booking_id){
        $booking = Booking::where('id', $booking_id)->first();

        $booking->delete();

        return redirect()->route('admin.getBookings');
    }
}
