$(document).ready(function(){
    $(document).on('click', '.showModalForm', function (){
        $('#modalForm').modal('show')
        $('#tableField').val($(this).attr('data-table'))
        $('#startField').val($(this).attr('data-start'))
        $('#endField').val($(this).attr('data-end'))
        $('#exampleModalLabel').text("" + $(this).attr('data-start-text') + "-" + $(this).attr('data-end-text'))
    })

    $(document).on('click', '.close',function (){
        $('#modalForm').modal('hide')
    })

    $(document).on('click', '.submitModalForm',function (){
        var name = $('#name').val()
        var phone = $('#phone').val()
        var email = $('#email').val()
        var table = $('#tableField').val()
        var start = $('#startField').val()
        var end = $('#endField').val()
        var url = $('.formBooking').attr('action')
        var token = $('#token').val()
        console.log(url)

        $.ajax({
            url: url,
            type: 'POST',
            data: {
                name: name,
                phone: phone,
                email: email,
                table: table,
                start: start,
                end: end,
                _token: token,
            },
            error: function (error){
                console.log(error.responseText)
            },
            success: function (data) {
                $('#modalForm').modal('hide')
                $('#' + data).remove()
            }
        })
    })

    $('.datepicker').datepicker({
        minDate: 0,
    })

    $('.datepicker').change(function (){
        var date = $('.datepicker').val()
        var venue = $(this).attr('data-venue')
        var venueName = $(this).attr('data-venue-name')
        var table = $(this).attr('data-table')
        var tableName = $(this).attr('data-table-name')
        var url = $(this).attr('data-url')

        $.ajax({
            url: url,
            type: 'GET',
            data: {
                day: date,
                venue: venue,
                table: table,
            },
            error: function (error){
                console.log(error.responseText)
            },
            success: function (data) {
                $('.slots').empty()
                data.forEach( function (value){
                    var startDate = new Date(value.start * 1000);
                    var startHour = startDate.getHours()
                    var startMinute = startDate.getMinutes()
                    if(startMinute < 10){
                        startMinute = "0" + startMinute
                    }

                    var endDate = new Date(value.end * 1000);
                    var endhour = endDate.getHours()
                    var endMinute = endDate.getMinutes()
                    if(endMinute < 10){
                        endMinute = "0" + endMinute
                    }

                    var text = startHour + ":" + startMinute + "-" + endhour + ":" + endMinute
                    var startText = startHour + ":" + startMinute
                    var endText = endhour + ":" + endMinute

                    $('.slots').append("<div class='col'><div class='card' style='width: 18rem; margin-top: 10px;' id='" + value.start + "'><img class='card-img-top' src='#' alt='" + text +"'><div class='card-body'><h5 class='card-title'>" + text +  "</h5><p class='card-text'><i>"+ venueName +" - "+ tableName +"</i></p><button type='button' class='btn btn-primary showModalForm' data-table='" + table + "' data-start='" + value.start + "' data-start-text='" + startText + "' data-end='" + value.end +"' data-end-text='" + endText + "'> Book </button> </div></div></div>")
                })
            }
        })
    })

    $('.deleteTable').click(function (){
        var deleteTable = confirm("Are you sure you want to delete this table?")
        if(deleteTable == true){
            window.location.href=$(this).attr('data-url')
        }
    })

    $('.deleteBooking').click(function (){
        var deleteBooking = confirm("Are you sure you want to delete this booking?")
        if(deleteBooking == true){
            window.location.href=$(this).attr('data-url')
        }
    })
})
