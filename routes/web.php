<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'namespace' => 'App\Http\Controllers',
], function() {
    Route::get('/', 'LoginController@show')->name('welcome');
    Route::get('/login', 'LoginController@show')->name('login');
    Route::post('/login', 'LoginController@postLogin')->name('postLogin');

    Route::get('logout', 'LoginController@logout')->name('logout');

    Route::group([
        'middleware' => 'auth',
    ], function () {
        Route::get('/dashboard', 'MainController@getDashboard')->name('getDashboard');

        Route::get('/venue/{id}', 'MainController@getVenue')->name('getVenue');

        Route::get('/venue/{venue_id}/booking/{table_id}', 'BookingController@getBookingSlots')->name('getBookingSlots');
        Route::post('/bookTable', 'BookingController@bookTable')->name('bookTable');
        Route::get('/newDate', 'BookingController@newDate')->name('newDate');

        Route::get('/bookings', 'BookingController@getBookings')->name('getBookings');
        Route::get('/deleteBooking/{id}', 'BookingController@getDeleteBooking')->name('getDeleteBooking');
    });
});




Route::group([
    'prefix' => '/admin',
    'namespace' => 'App\Http\Controllers\Admin',
    'as' => 'admin.'
], function() {
    Route::group([
        'middleware' => 'guestAdmin:admin',
    ], function () {
        Route::get('/login', 'LoginController@show')->name('login');
        Route::post('login', 'LoginController@validateAuth')->name('validate_login');
    });

    Route::get('logout', 'LoginController@logout')->name('logout');

    Route::group([
        'middleware' => ['authAdmin:admin', 'role:Admin']
    ], function() {
        Route::get('/', 'DashController@index')->name('home');
        Route::get('/dash', 'DashController@index')->name('dash');

        Route::get('/venues', 'VenuesController@getVenues')->name('getVenues');
        Route::get('/venue/{id}', 'VenuesController@getVenue')->name('getVenue');

        Route::get('/addVenue', 'VenuesController@getAddVenue')->name('getAddVenue');
        Route::post('/addVenue', 'VenuesController@postAddVenue')->name('postAddVenue');

        Route::get('/venue/{id}/settings', 'VenuesController@getVenueSettings')->name('getVenueSettings');
        Route::post('/venue/{id}/settings', 'VenuesController@postVenueSettings')->name('postVenueSettings');

        Route::get('/venue/{venue_id}/newTable', 'TablesController@getAddTable')->name('getAddTable');
        Route::post('/venue/{venue_id}/newTable', 'TablesController@postAddTable')->name('postAddTable');
        Route::get('/deleteTable/{id}', 'TablesController@getDeleteTable')->name('getDeleteTable');

        Route::get('/venue/{venue_id}/booking/{table_id}', 'BookingController@getBookingSlots')->name('getBookingSlots');
        Route::post('/bookTable', 'BookingController@bookTable')->name('bookTable');
        Route::get('/newDate', 'BookingController@newDate')->name('newDate');

        Route::get('/bookings', 'BookingController@getBookings')->name('getBookings');
        Route::get('/deleteBooking/{id}', 'BookingController@getDeleteBooking')->name('getDeleteBooking');
    });

});
